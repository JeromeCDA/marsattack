package fr.afpa.marsattack.model;

import junit.framework.TestCase;

public class GestionSpaceshipServicesTest extends TestCase {

	GestionSpaceshipServices gSpaceship;
	
	protected void setUp() throws Exception {
		super.setUp();
		gSpaceship = new GestionSpaceshipServices();
		gSpaceship.getSpaceship().setLeftSide(10);
		gSpaceship.getSpaceship().setRightSide(55);
		gSpaceship.getSpaceship().setTopSide(10);
		gSpaceship.getSpaceship().setBottomSide(55);
		
		gSpaceship.getSpaceship().setXPos(10);
		gSpaceship.getSpaceship().setYPos(10);
		
		gSpaceship.getSpaceship().setHeight(50);
		gSpaceship.getSpaceship().setWidth(50);
		
		gSpaceship.getSpaceship().setSpeed(10);
		gSpaceship.getSpaceship().setLife(0);
		
		gSpaceship.getSpaceship().setUn(true);
		gSpaceship.getSpaceship().setDead(true);


	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	public void testLooseLife() {

	
		assertTrue(gSpaceship.getSpaceship().isDead());
	
	}

	public void testCalculedBox() {

		assertEquals(gSpaceship.getSpaceship().getLeftSide(), gSpaceship.getSpaceship().getXPos());
		assertEquals(gSpaceship.getSpaceship().getRightSide(), gSpaceship.getSpaceship().getXPos()+(gSpaceship.getSpaceship().getWidth()-5));
		assertEquals(gSpaceship.getSpaceship().getTopSide(), gSpaceship.getSpaceship().getYPos());
		assertEquals(gSpaceship.getSpaceship().getBottomSide(), gSpaceship.getSpaceship().getYPos()+ (gSpaceship.getSpaceship().getHeight()-5));
	}
	
	public void testAnimatedDeath() {

		assertFalse(gSpaceship.animatedDeath());
		
//
		
	}
	
	
	public void testGetXPos() {
		assertNotNull(gSpaceship.getSpaceship().getXPos());
	}

	public void testGetYPos() {
		assertNotNull(gSpaceship.getSpaceship().getYPos());
	}

	public void testGetHeight() {
		assertNotNull(gSpaceship.getSpaceship().getHeight());
	}

	public void testGetWidth() {
		assertNotNull(gSpaceship.getSpaceship().getWidth());
	}

	public void testGetLeftSide() {
		assertNotNull(gSpaceship.getSpaceship().getLeftSide());
	}

	public void testGetRightSide() {
		assertNotNull(gSpaceship.getSpaceship().getRightSide());
	}

	public void testGetTopSide() {
		assertNotNull(gSpaceship.getSpaceship().getTopSide());
	}

	public void testGetBottomSide() {
		assertNotNull(gSpaceship.getSpaceship().getBottomSide());
	}

}
