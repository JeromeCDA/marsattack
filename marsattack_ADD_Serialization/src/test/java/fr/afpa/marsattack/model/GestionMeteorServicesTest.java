package fr.afpa.marsattack.model;

import fr.afpa.marsattack.beans.FireMeteor;
import fr.afpa.marsattack.beans.IceMeteor;
import fr.afpa.marsattack.beans.Meteor;
import fr.afpa.marsattack.beans.ZigZagMeteor;
import fr.afpa.marsattack.resources.Configuration;
import junit.framework.TestCase;

public class GestionMeteorServicesTest extends TestCase {

	GestionMeteorServices gMeteors;
	GestionSpaceshipServices gSpaceship;
	Meteor meteor;
	private int boardHeight;
	
	
	protected void setUp() throws Exception {
		super.setUp();
		gSpaceship = new GestionSpaceshipServices();
		gMeteors = new GestionMeteorServices(gSpaceship);
		meteor = new Meteor();
		
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void getMeteors() {
		assertEquals(6, gMeteors.getMeteors().size());
		assertNotNull(gMeteors.getMeteors());
	}
	
	public void testUpdate() {
		boardHeight = -12;
		int id = -1;
		boolean remove = false;
		for (int i = 0; i < gMeteors.getMeteors().size(); i++) {
			gMeteors.getMeteors().get(i).update();
		}
		boardHeight = -12;
		for (int i = 0; i < gMeteors.getMeteors().size(); i++) {
			if (gMeteors.getMeteors().get(i).getYPos() > boardHeight) {
				id = i;
				gSpaceship.getSpaceship().setScore(gSpaceship.getSpaceship().getScore()+gMeteors.getMeteors().get(i).getPoints());
				remove =true;
			}
		}
		if(remove) {
			gMeteors.removeMeteors(id);
			assertEquals(5, gMeteors.getMeteors().size());
			gMeteors.addMeteor();
			assertEquals(6, gMeteors.getMeteors().size());
		}
	}
	
	public void Collision() {
		gSpaceship.calculedBox();
		meteor.calculedBox();
		meteor.setTopSide(46);
		gSpaceship.getSpaceship().setBottomSide(12);

		assertFalse(gMeteors.collision(gSpaceship, meteor));
		
		
		meteor.setRightSide(12);
		gSpaceship.getSpaceship().setLeftSide(45);

		assertFalse(gMeteors.collision(gSpaceship, meteor));

			
	}

	public void testRemoveMeteors() {
		gMeteors.removeMeteors(1);
		assertEquals(5,gMeteors.getMeteors().size());
	}

	public void testAddMeteor() {
		assertEquals(6, gMeteors.getMeteors().size());
	}

	public void testRandMeteor() {
		assertNotNull(gMeteors.randMeteor(12, 42));
	}

}
