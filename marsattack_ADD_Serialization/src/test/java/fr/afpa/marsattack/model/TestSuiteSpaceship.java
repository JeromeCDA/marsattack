package fr.afpa.marsattack.model;

import junit.extensions.ActiveTestSuite;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class TestSuiteSpaceship extends TestCase {

    public TestSuiteSpaceship(String name) {
        super(name);
    }


    public static TestSuite suite() {
        TestSuite suite = new ActiveTestSuite();
        suite.addTest(new TestSuite(GestionSpaceshipServicesTest.class));
        return suite;
    }

    public static void main(String [] args) {
        junit.textui.TestRunner.run(suite());
    }

}
