package fr.afpa.marsattack.beans;

import junit.framework.TestCase;

public class MeteorTest extends TestCase {

	Meteor meteor;
	
	protected void setUp() throws Exception {
		super.setUp();
		meteor = new Meteor();
		meteor.setLeftSide(10);
		meteor.setRightSide(50);
		meteor.setTopSide(10);
		meteor.setBottomSide(50);
		
		meteor.setXPos(10);
		meteor.setYPos(10);
		
		meteor.setHeight(50);
		meteor.setWidth(50);
		
		meteor.setSpeed(10);
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		meteor = null;
	}

	public void testCalculedBox() {
		assertEquals(meteor.getLeftSide(), meteor.getXPos());
		assertEquals(meteor.getRightSide(), meteor.getXPos()+meteor.getWidth()-10);
		assertEquals(meteor.getTopSide(), meteor.getYPos());
		assertEquals(meteor.getBottomSide(), meteor.getYPos()+ meteor.getHeight()-10);
	}

	public void update() {
		meteor.calculedBox();
		assertNotSame(meteor.getYPos(), meteor.getYPos()+meteor.getSpeed());
	}
	

	
	public void randXYMeteor() {
		assertNotNull(meteor.randXYMeteor(12, 42));
	}
	
	public void testGetXPos() {
		assertNotNull(meteor.getXPos());
	}

	public void testGetYPos() {
		assertNotNull(meteor.getYPos());
	}

	public void testGetHeight() {
		assertNotNull(meteor.getHeight());
	}

	public void testGetWidth() {
		assertNotNull(meteor.getWidth());
	}

	public void testGetLeftSide() {
		assertNotNull(meteor.getLeftSide());
	}

	public void testGetRightSide() {
		assertNotNull(meteor.getRightSide());
	}

	public void testGetTopSide() {
		assertNotNull(meteor.getTopSide());
	}

	public void testGetBottomSide() {
		assertNotNull(meteor.getBottomSide());
	}

}
