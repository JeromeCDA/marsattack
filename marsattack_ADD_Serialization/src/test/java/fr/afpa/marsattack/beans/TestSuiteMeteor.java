package fr.afpa.marsattack.beans;

import fr.afpa.marsattack.model.GestionMeteorServicesTest;
import fr.afpa.marsattack.model.GestionSpaceshipServicesTest;
import junit.extensions.ActiveTestSuite;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class TestSuiteMeteor extends TestCase {

	public TestSuiteMeteor(String name) {
		super(name);
	}


	public static TestSuite suite() {
		TestSuite suite = new ActiveTestSuite();
		suite.addTest(new TestSuite(MeteorTest.class));
		suite.addTest(new TestSuite(GestionMeteorServicesTest.class));
		suite.addTest(new TestSuite(GestionSpaceshipServicesTest.class));
		return suite;
	}
	
	public static void main(String [] args) {
		junit.textui.TestRunner.run(suite());
	}

}
