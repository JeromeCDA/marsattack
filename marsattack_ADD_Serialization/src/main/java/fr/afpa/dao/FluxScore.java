package fr.afpa.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

import fr.afpa.marsattack.beans.Player;





public  class FluxScore implements Serializable {


    private String file;

    public FluxScore() {
       
        File file = new File(".\\temp");
        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Directory is created!");
            } else {
                System.out.println("Failed to create directory!");
            }
        }
        this.file = ".\\temp\\Scores.cda";
    }


    public  void saveScore(Player player) {
        // Ecriture en utilisant la serialisation
        try {
            FileOutputStream fos = new FileOutputStream(file, true);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(player);
            oos.close();
            fos.close();
            oos.flush();
        } catch (Exception e) {
            System.out.println("Erreur " + e);
        } 

    }


    public ArrayList<Player> retrieveScores(){
        ArrayList<Player> listScores = new ArrayList();
        boolean ok = true;

            try {
                FileInputStream fis = new FileInputStream(file);
               ObjectInputStream ois = null;
                Player newPlayer = null;
                while (ok) {
                        try {
                        	System.out.println(ok);
                        	ois = new ObjectInputStream(fis);
                            newPlayer = (Player) ois.readObject();
                            System.out.println("While "+newPlayer );
                            listScores.add(newPlayer);
                        } catch (Exception ex) {

                            break;
                        } 
                }

               

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            
            Collections.sort(listScores, (a,b) -> b.compareTo(a));
            
            
            System.out.println("Ma liste dans le flux"+ listScores);
        return listScores;
    }


}