package fr.afpa.marsattack.model;

import java.awt.Graphics2D;

import fr.afpa.marsattack.beans.Spaceship;
import fr.afpa.marsattack.resources.Configuration;
import fr.afpa.marsattack.vue.GameScene;
import lombok.Getter;
import lombok.Setter;



@Getter
@Setter
public class GestionSpaceshipServices {

	private Spaceship spaceship;

	public GestionSpaceshipServices() {
		spaceship = new Spaceship();
	}
	
	
	public void update() {
		calculedBox();
		move();
	}
	
	/**
	 * Permet de calculer le carré du vaisseau pour la gestion des collision.
	 */
	public void calculedBox() {
		spaceship.setLeftSide(spaceship.getXPos());
		spaceship.setTopSide(spaceship.getYPos());
		spaceship.setRightSide(spaceship.getXPos() + (spaceship.getWidth()-5));
		spaceship.setBottomSide(spaceship.getYPos() + (spaceship.getHeight()-5));	
	}
	
	// permet de déplacement du vaisseau. a chaque déplacement, cette méthode gère également le changement d'image du vaisseau.
	public void move() {
		if( spaceship.getXPos() > 0) {
			if(spaceship.isLeft()) {
				spaceship.setDx(-spaceship.getSpeed());
				spaceship.setImage( spaceship.getImgMov2());
			}
		}
		if(spaceship.isRight()) {
			spaceship.setDx(spaceship.getSpeed());
			spaceship.setImage( spaceship.getImgMov3());
		}
		if( spaceship.getYPos() > 0) {
			if(spaceship.isUp()) {
				spaceship.setDy(-spaceship.getSpeed());
				spaceship.setImage( spaceship.getImgMov1());
			}
		}
		if(spaceship.isDown()) {
			spaceship.setDy(spaceship.getSpeed());
			spaceship.setImage( spaceship.getImgMov1());
		}
		spaceship.setXPos(spaceship.getXPos() + spaceship.getDx());
		spaceship.setYPos(spaceship.getYPos() + spaceship.getDy());
		
		if( spaceship.getXPos() > (Configuration.BOARD_WIDTH - spaceship.getWidth())) spaceship.setXPos(Configuration.BOARD_WIDTH - spaceship.getWidth());
		if( spaceship.getYPos() > Configuration.BOARD_HEIGHT - spaceship.getHeight()) spaceship.setYPos(Configuration.BOARD_WIDTH - spaceship.getHeight());
		
		spaceship.setDx(0); 
		spaceship.setDy(0); 
		
		long elapsedTime = (System.nanoTime() - spaceship.getRecoveryTimer()) / 1000000; 
		if( elapsedTime > 500) {
			spaceship.setCollisionMeteor(false);
			spaceship.setRecoveryTimer(0);
		}
	}
	
	// Permet le dessin du vaisseau.
	public void draw(Graphics2D g) {
		
		if (spaceship.isCollisionMeteor()) { 
			g.drawImage(spaceship.getCollisionImg(), spaceship.getXPos(), spaceship.getYPos(), 70,70, null);
		} else {
			g.drawImage(spaceship.getImage(), spaceship.getXPos(), spaceship.getYPos(),  70,70, null);
		}
	}
	
	// gère la perte de vie et la mort du vaisseau
	public void looseLife() {
		spaceship.setCollisionMeteor(true); 
		spaceship.setRecoveryTimer(System.nanoTime()); 
		if ( spaceship.getLife() < 1) {
			spaceship.setDead(true); 
		}
	}
	
	// Gere l'animation de la destruction du vaisseau.
	public boolean animatedDeath() {
		if (spaceship.isUn()) {
			spaceship.setImage(spaceship.getDest1());
			spaceship.setUn(false);
			spaceship.setDeux(true);
		}else if(spaceship.isDeux()) {
			spaceship.setImage(spaceship.getDest2());
			spaceship.setTrois(true);
			spaceship.setDeux(false);
		}else if (spaceship.isTrois()) {
			spaceship.setImage(spaceship.getDest3()) ;
			spaceship.setQuatre(true);
			spaceship.setTrois(false); 
		}else if(spaceship.isQuatre()) {
			spaceship.setImage(spaceship.getDest4()) ;
			spaceship.setQuatre(false);
			spaceship.setCinq(true); 
		}else if(spaceship.isCinq()) {
			spaceship.setImage(spaceship.getDest5()) ;
			spaceship.setCinq(false);
			spaceship.setSix(true); 
		}else if(spaceship.isSix()) {
			spaceship.setImage(spaceship.getDest6());
			spaceship.setSix(false); 
			spaceship.setSept(true);
		}else if(spaceship.isSept()) {
			spaceship.setImage(spaceship.getDest7()) ;
			spaceship.setSept(false);
			spaceship.setHuit(true); 
		}else if(spaceship.isHuit()) {
			spaceship.setImage(spaceship.getDest8());
			spaceship.setHuit(false); 	
		} else {
			spaceship.setImage(spaceship.getBlank()); 
			spaceship.setUn(true);
			return true;
		}
		return false;
	}
		
	
}
