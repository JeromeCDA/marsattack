package fr.afpa.marsattack.model;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import fr.afpa.marsattack.beans.FireMeteor;
import fr.afpa.marsattack.beans.IceMeteor;
import fr.afpa.marsattack.beans.Meteor;
import fr.afpa.marsattack.beans.ZigZagMeteor;
import fr.afpa.marsattack.resources.Configuration;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
/**
 * Classe métier des Météores.
 * P
 * @author NEBULA
 *
 */
public class GestionMeteorServices {

	private List<Meteor> meteors;
	private GestionSpaceshipServices gSpaceship;
	
	public GestionMeteorServices(GestionSpaceshipServices gSpaceship) {
		this.gSpaceship = gSpaceship;
		meteors = new ArrayList();
		for ( int i=0 ; i<6 ; i++) {
			addMeteor();
		}
	}

	
	public void draw(Graphics2D g) {
		for ( int i = 0; i < 6; i++) {
			this.meteors.get(i).draw(g);
		}
	}
	
	// Récupère l'update de chaque météores et renouvelle la liste si un météores est passée hors du champs de jeu. Elle incrémente également les points de score lors de la destruction.
	public void update() {
		int id = -1;
		boolean remove = false;
		for (int i = 0; i < meteors.size(); i++) {
			meteors.get(i).update();
		}
		for (int i = 0; i < meteors.size(); i++) {
			if (meteors.get(i).getYPos() > Configuration.BOARD_HEIGHT) {
				id = i;
				gSpaceship.getSpaceship().setScore(gSpaceship.getSpaceship().getScore()+meteors.get(i).getPoints());
				remove =true;
			}
		}
		if (remove) {
			removeMeteors(id);
			addMeteor();
		}
		id = -1;
	}

	

	public void removeMeteors(int id) {
		meteors.remove(id);
		
	}
	
	// ajoute des météores de manière random.
	public void addMeteor() {
		int nb = randMeteor(1,4);
		if(nb == 1) {
			meteors.add(new Meteor());
		}else if (nb == 2) {
			meteors.add(new FireMeteor());
		}else if (nb == 3) {
			meteors.add(new IceMeteor());
		}else if (nb == 4) {
			meteors.add(new ZigZagMeteor());
		}
	}
	
	
	// gère la suppression des météores et l'ajout dans la liste lors d'une collision
	public boolean gestionCollision() {
		for(int i = 0; i < getMeteors().size(); i++) {
			if (collision(gSpaceship , getMeteors().get(i))) {
				removeMeteors(i);
				addMeteor();
				return true;
			}
			
		}
		return false;
	}
	
	// gere la collision entre une météore et le vaisseau.
	public boolean collision(GestionSpaceshipServices gSpaceship, Meteor meteor) {
		gSpaceship.calculedBox();
		meteor.calculedBox();

		// Collision du vaisseau par le haut
		if(meteor.getBottomSide() <= gSpaceship.getSpaceship().getTopSide()) {
			return false;
		}
			
		//Collisiondi vaisseau par le bas
		if(meteor.getTopSide() >=  gSpaceship.getSpaceship().getBottomSide()) {
			return false;
		}
			
		// Collision du vaisseau par la droite
		if (meteor.getRightSide() <=  gSpaceship.getSpaceship().getLeftSide()) {
			return false;
		}
		// Collision du vaisseau par la gauche
		if (meteor.getLeftSide() >=  gSpaceship.getSpaceship().getRightSide()) {
			return false;
		}
		gSpaceship.getSpaceship().setLife( gSpaceship.getSpaceship().getLife() - meteor.getImpact());
		gSpaceship.looseLife();
		return true;
	}
	
	
	// permet l'animation de chaque métérores de la liste.
	public void animatedMeteor() {
		for(int i = 0; i < getMeteors().size(); i++) {
			getMeteors().get(i).animatedImages();
		}		
	}
	
	public int randMeteor(int x1, int x2) {
		Random r = new Random();
		return r.nextInt((x2 - x1) + 1) + x1;
	}
}
