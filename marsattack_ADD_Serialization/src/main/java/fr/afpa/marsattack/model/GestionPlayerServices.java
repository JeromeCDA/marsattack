package fr.afpa.marsattack.model;



import java.util.ArrayList;

import fr.afpa.dao.FluxScore;
import fr.afpa.marsattack.beans.Player;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GestionPlayerServices {

	Player player;
	
	public void sendScoreToDao(Player player) {
        FluxScore sendScore = new FluxScore();
        sendScore.saveScore(player);
    }
	
	
	public ArrayList <Player> retrieveScoresFromDAO(){
        FluxScore flux = new FluxScore();
        return flux.retrieveScores();
    }
	
	
}
