package fr.afpa.marsattack.resources;

/**
 * Classe de configuration regroupant les constantes du jeu.
 * @author NEBULA
 *
 */
public class Configuration {
	// Le titre de notre jeu
	public static final String TITLE = "Space Invasion";
	
	// La hauteur et largeur de notre GameMainFrame et GamePanel
	public static final int BOARD_WIDTH =564;
	public static final int BOARD_HEIGHT = 600;
	public static final int BOARD_MARGE = 50;
	
	
	// La vitesse de refresh de l'application
	public static final int FPS = 30;
	
	
	//SPACESHIP
	// taille du spaceShip
	public static final int SPACESHIP_WIDTH = 70;
	public static final int SPACESHIP_HEIGTH = 70;
	public static final int SPACESHIP_LIFE = 1;
	//position initiale du spaceShip
	public static final int X_POS_INIT_SHIP = (BOARD_WIDTH - BOARD_HEIGHT)/2;
	public static final int Y_POS_INIT_SHIP = 490;
	//Déplacement  du spaceShip
	public static final int DEP_X = 1;
	public static final int DEP_Y = 1;
	//Limite déplacement vaisseau
	public final static int LEFT_LIMIT_SHIP = 60;
	public final static int RIGHT_LIMIT_SHIP = 500;
	public final static int UP_LIMIT_SHIP = 10;
	public final static int DOWN_LIMIT_SHIP = 450;
	
	//BasicMeteor
	// taille du spaceShip
	public static final int B_METEOR_WIDTH =60;
	public static final int B_METEOR_HEIGTH = 60;
	public static final int DEP_Y_BASIC_METEOR = 1;
	public static final int VITESSE_BASIC_METEOR = 1;


}
