package fr.afpa.marsattack.controller;

import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import fr.afpa.marsattack.model.GestionMeteorServices;
import fr.afpa.marsattack.model.GestionSpaceshipServices;
import fr.afpa.marsattack.vue.Game;
import fr.afpa.marsattack.vue.GameScene;


/**
 * COntroller du spaceShip.
 * Passe l'update, le draw la mort et la collision mais également le mouvement du spaceship.
 * @author NEBULA
 *
 */
public class SpaceshipController implements KeyListener {
	
	private	Game game;
	GestionSpaceshipServices gSpaceship;
	GestionMeteorServices gMeteors;
	GameScene gameScene;
	private int counter = 0;

	public SpaceshipController(GestionSpaceshipServices gSpaceship, GestionMeteorServices gMeteors,  GameScene gameScene, Game game) {
		this.game = game;
		this.gSpaceship = gSpaceship;
		this.gMeteors = gMeteors;
		this.gameScene = gameScene;
	}
		
	
	
	public void spaceshipUpdate() {
		System.out.println(Thread.currentThread()+" Life "+gSpaceship.getSpaceship().getLife());
		gSpaceship.update();
	}
	public void spaceshipDraw(Graphics2D g) {
		gSpaceship.draw(g);
	}
	public void spaceshipCollision() {
		if(!gSpaceship.getSpaceship().isCollisionMeteor()) {
			gMeteors.gestionCollision();
		}
	}
	
	public boolean checkSpaceshipDeath(){
		
		return gSpaceship.getSpaceship().isDead();

	}
	
	
		@Override
	public void keyPressed(KeyEvent key) {
		int keyCode = key.getKeyCode();
		if( keyCode == KeyEvent.VK_LEFT) {
			gSpaceship.getSpaceship().setLeft(true);
		}
		if( keyCode == KeyEvent.VK_RIGHT) {
			gSpaceship.getSpaceship().setRight(true);
		}
		if( keyCode == KeyEvent.VK_UP) {
			gSpaceship.getSpaceship().setUp(true);
		}
		if( keyCode == KeyEvent.VK_DOWN) {
			gSpaceship.getSpaceship().setDown(true);
		}
	}

	@Override
	public void keyReleased(KeyEvent key) {
		int keyCode = key.getKeyCode();
		if( keyCode == KeyEvent.VK_LEFT) {
			gSpaceship.getSpaceship().setLeft(false);
		}
		if( keyCode == KeyEvent.VK_RIGHT) {
			gSpaceship.getSpaceship().setRight(false);
		}
		if( keyCode == KeyEvent.VK_UP) {
			gSpaceship.getSpaceship().setUp(false);
		}
		if( keyCode == KeyEvent.VK_DOWN) {
			gSpaceship.getSpaceship().setDown(false);
		}
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	


}
