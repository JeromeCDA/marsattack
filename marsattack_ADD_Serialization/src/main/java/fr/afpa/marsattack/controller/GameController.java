package fr.afpa.marsattack.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;

import fr.afpa.marsattack.beans.Player;
import fr.afpa.marsattack.model.GestionPlayerServices;
import fr.afpa.marsattack.model.GestionSpaceshipServices;
import fr.afpa.marsattack.vue.Game;
import fr.afpa.marsattack.vue.GameScene;
import fr.afpa.marsattack.vue.UiNewPlayer;


/**
 * Est le controller de la classe Game.
 * Permet vérifier et de lancer des actions si le jeu est GameOver.
 * @author NEBULA
 *
 */
public class GameController implements ActionListener {

	private GestionPlayerServices gPlayer;
	private GestionSpaceshipServices gSpaceship;
	private Game game;
	//private UiNewPlayer newPlayer;

	
	public GameController(GestionSpaceshipServices gSpaceship, Game game, GestionPlayerServices gPlayer) {
		this.gSpaceship = gSpaceship;
		this.game = game;
		this.gPlayer = gPlayer;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {


		if(GameScene.gameOver) {
			gameOver();
		}
		System.out.println(e.getSource());
		if (e.getSource()instanceof JButton) {
			 if ( "Restart".equals(((JButton)e.getSource()).getText())){
				 restart();
			 } else if ( "Exit".equals(((JButton)e.getSource()).getText())) {
				 System.exit(0);
			 } else if ( "Score".equals(((JButton)e.getSource()).getText())) {
				if (!isScoreFileEmpty()) {
					System.out.println("GAMELOOP");
					game.loopThroughScoresList();
				}else {
					game.noScoreToDisplay();
				}
				 System.out.println("Ajouter score !");
			 } 
		}
		
	}
	 

	
	public boolean isScoreFileEmpty() {

        boolean verif = false;
        if(gPlayer.retrieveScoresFromDAO().isEmpty()) {
            verif = true;
            //faire un JOptionPane pour informer qu'il n'y a pas encore de scores
        }
        return verif;
    }
	
    public ArrayList <Player> retrieveScoresFromModel(){
        return gPlayer.retrieveScoresFromDAO();
    }


	
	public void gameOver() {
		if(GameScene.gameOver) {
			game.getTimerGameOver().setRepeats(false);
			game.setContentPane(game.getPanelGameOver());
			game.getGameScene().setVisible(false);
		}
		if (!game.getTimerGameOver().isRunning() && game.getGPlayer().getPlayer() != null) {
			game.getGPlayer().getPlayer().setScore(gSpaceship.getSpaceship().getScore());
			game.getGPlayer().sendScoreToDao(game.getGPlayer().getPlayer());
			game.getGPlayer().setPlayer(null);
		}
	}
	
	public void restart() {

		game.getRestart().setEnabled(false);
		game.getExit().setEnabled(false);
		game.getScore().setEnabled(false);
		GameScene.gameOver = false ;
		game.gameInit();
	}
	

	
	public boolean checkPlayerName(String name) {
		boolean verifName = false;
		String regex = "[^;.]{3,6}";
		if (name.matches(regex)) {
			verifName = true;
			game.getGPlayer().setPlayer(new Player(name));
		} 
		return verifName;
	}
}
