package fr.afpa.marsattack.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import fr.afpa.marsattack.model.GestionMeteorServices;
import fr.afpa.marsattack.model.GestionSpaceshipServices;
import fr.afpa.marsattack.vue.GameScene;


// permet de gérer l'animation de l'explosion et de lancer le top de fin du jeu en mettant GameOver à True.
public class AnimationController implements ActionListener {

	private GameScene gameScene;
	private GestionSpaceshipServices gSpaceship;
	
	
	public AnimationController(GameScene gameScene, GestionSpaceshipServices gSpaceship, GestionMeteorServices gMeteors) {
		this.gameScene = gameScene;
		this.gSpaceship = gSpaceship;

	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(gSpaceship.getSpaceship().isDead()) {
			animatedDeathShip();
		}
		
	}
	
	public void animatedDeathShip() {
		if (gSpaceship.animatedDeath()) {
			gameScene.gameOver = true;
		}
		
	}

}
