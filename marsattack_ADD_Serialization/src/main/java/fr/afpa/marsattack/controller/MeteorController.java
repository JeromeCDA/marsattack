package fr.afpa.marsattack.controller;

import java.awt.Graphics2D;

import fr.afpa.marsattack.model.GestionMeteorServices;
import fr.afpa.marsattack.model.GestionSpaceshipServices;
import fr.afpa.marsattack.vue.GameScene;

/**
 * Controller de Météores
 */
public class MeteorController {
	
	
	private GestionMeteorServices gMeteors;
	public GameScene gameScene;
	
	
	public MeteorController(GestionSpaceshipServices gSpaceship, GestionMeteorServices gMeteors, SpaceshipController spaceshipControl, GameScene gameScene) {
		this.gMeteors = gMeteors;
		this.gameScene = gameScene;
				
	}


	public void updateMeteors() {
		gMeteors.update();
	}


	public void meteorsDraw(Graphics2D g) {
		gMeteors.draw(g);
		
	}
	
	
	
	
}
