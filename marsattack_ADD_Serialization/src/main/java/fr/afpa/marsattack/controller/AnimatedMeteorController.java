package fr.afpa.marsattack.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import fr.afpa.marsattack.model.GestionMeteorServices;
import fr.afpa.marsattack.vue.GameScene;

/**
 * Permet d'enclencher les animations de météores
 * @author NEBULA
 *
 */
public class AnimatedMeteorController implements ActionListener {
	
	private	GameScene gameScene;
	private GestionMeteorServices meteors;
	
	public AnimatedMeteorController(GameScene gameScene,GestionMeteorServices meteors ) {
		this.gameScene = gameScene;
		this.meteors = meteors;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		this.meteors.animatedMeteor();
	}

}
