package fr.afpa.marsattack.vue;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Classe permettant le refresh de la classe GameScene
 * @author NEBULA
 *
 */
public class RefreshTimer implements ActionListener{

	private	GameScene gameMain;
	
	public RefreshTimer(GameScene gameMain) {
		this.gameMain = gameMain;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		this.gameMain.refresh();
	}

}
