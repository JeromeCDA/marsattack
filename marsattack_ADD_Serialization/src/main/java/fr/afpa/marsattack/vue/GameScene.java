package fr.afpa.marsattack.vue;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

import fr.afpa.marsattack.controller.AnimatedMeteorController;
import fr.afpa.marsattack.controller.SpaceshipController;
import fr.afpa.marsattack.controller.AnimationController;
import fr.afpa.marsattack.controller.MeteorController;
import fr.afpa.marsattack.model.GestionMeteorServices;
import fr.afpa.marsattack.model.GestionSpaceshipServices;
import fr.afpa.marsattack.resources.Configuration;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
/**
 * Cette classe est l'affichage du déroulement du jeu. Elle possede plusieurs timer permettant d'effectuer plusieurs tâches en parallèle.
 * Le principal est le timer Refresh qui va permettre au jeu de faire un rafraichissement en permanence.
 * Deux autres timers sont égalements instanciés. Le timer AnimationDeathShip qui permet de gérer l'aninamtion de l'explosion du vaisseau
 * le timer AnimationMétéor sert lui a gérer l'animation en permanence des métérores.
 * 
 * 
 * @author NEBULA
 *
 */
public class GameScene extends JPanel {

	private BufferedImage image;
	private Graphics2D g;

	private GestionSpaceshipServices spaceship;
	private GestionMeteorServices meteors;
	private SpaceshipController spaceshipControl;
	private MeteorController meteorsControl;
	
	private  Image life;

	private long startTimer;
	private long startTimerDiff;
	private boolean start;
	private int startDelay;
	
	public static boolean gameOver;
	
	private  Timer timerRefresh; 
	private Timer timerAnimationMeteor;
	private Timer timerAnimationDeathShip;
	private  int counter;
	
	public Image backgroundImg;
	public int yBackBis;
	public int yBack;
	
	

	/**
	 * Constructeur de la classe gameScene. Il a en paramettre les models spacship et météors, ainsi que les controller. le constructeur est découpé en plusieurs méthode pour améliorer sa clarté.
	 * @param spaceship
	 * @param meteors
	 * @param spaceshipControl
	 * @param meteorsControl
	 */
	public GameScene(GestionSpaceshipServices spaceship, GestionMeteorServices meteors, SpaceshipController spaceshipControl, MeteorController meteorsControl) {
		super();

		initVariables(spaceship, meteors, spaceshipControl, meteorsControl);
		initImages();
		setPreferredSize(new Dimension(Configuration.BOARD_WIDTH, Configuration.BOARD_HEIGHT));
		setFocusable(true);
		requestFocus();


	}
	
	/**
	 * Cette méthode est appelée dans le constructeur. Elle s'occupe de l'initialisation des variables, classe et timer.
	 * @param spaceship
	 * @param meteors
	 * @param spaceshipControl
	 * @param meteorsControl
	 */
	public void initVariables(GestionSpaceshipServices spaceship, GestionMeteorServices meteors, SpaceshipController spaceshipControl, MeteorController meteorsControl) {
		image = new BufferedImage(Configuration.BOARD_WIDTH, Configuration.BOARD_HEIGHT, BufferedImage.TYPE_INT_RGB);
		g = (Graphics2D) image.getGraphics();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		
		counter = 0;
		startDelay = 2000;
		this.spaceship = spaceship;
		this.meteors = meteors;
		this.spaceshipControl = spaceshipControl;
		this.meteorsControl = meteorsControl;
		gameOver = false;
		this.timerAnimationDeathShip = new Timer(200, new AnimationController(this, spaceship, meteors));
		this.timerAnimationMeteor = new Timer(100, new AnimatedMeteorController(this, meteors ));
		timerAnimationMeteor.start();
		this.timerRefresh = new Timer (30, new RefreshTimer(this) );
		this.timerRefresh.start();
	}
	
	
	/**
	 * Cette méthode est appelée dans le constructeur. Elle permet l'initialisation du timer artificiel permettant l'animation du NEW GAME,  du background et de l'image représentant la vie du joueur.
	 */
	private void initImages() {
		// timer new Game
		startTimer = 0;
		startTimerDiff = 0;
		start = true;
		
		// coeurs de vie
		ImageIcon image = new ImageIcon(getClass().getResource("/SpaceShip/life.png"));
		life = image.getImage(); 
		
		// background
		image = new ImageIcon(getClass().getResource("/back.gif"));
		backgroundImg = image.getImage();
		yBack = -312; yBackBis = -312;
		

		
	}

	
	
//------------------------------------------------------- GAME METHODS------------------------------------------------	

	/**
	 * Cette méthode permets de peindre les composant qu'elle intègre. 
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		
		background(g);
		drawStartGame(g);
		spaceshipControl.spaceshipDraw(g2);
		meteorsControl.meteorsDraw(g2);
		lives(g);
		drawScrore(g);
	}

	/**
	 * Cette méthode est appelée par le timer toutes les 30mS. Celle ci utilise le repaint permettant de repeindre les composants.
	 */
	public void refresh() {
		update();
		repaint();	
		
	}
	
	/**
	 * Cette méthode intègre toutes les méthode dynamique pas forcément en rapport direct avec le dessin. 
	 */
	public void update() {
		newGame();
		spaceshipControl.spaceshipUpdate();
		meteorsControl.updateMeteors();
		spaceshipControl.spaceshipCollision();
		checkDeadPlayer();
	}

	
	//--------------------------------------METHODES D'AFFICHAGE appelées dans le PaintComponent()-----------------------------------------------------------
	
	
	/**
	 * Définit le background a peindre
	 * @param g2
	 */
	public void background(Graphics g) {

		System.out.println(yBack);

		g.drawImage(backgroundImg,  0, yBack, 564,1002, null);
		if(yBack>0) {
			g.drawImage(backgroundImg,  0, yBackBis, 564,1002, null);
			yBackBis++;
			if(yBackBis > 0) {
				yBack = -312;
			}
		}
		yBack++;
	}

	
	/**
	 * definit les scores dans l'affichage
	 * @param g
	 */
	public void drawScrore(Graphics g) {
		g.setColor(Color.white);
		g.setFont(new Font("Spaceboy", Font.PLAIN, 20));
		g.drawString("Score : "+spaceship.getSpaceship().getScore() , Configuration.BOARD_WIDTH -180 , 45);
	}
	
	/**
	 * définit les vies dans l'affichage
	 * @param g
	 */
	public void lives(Graphics g) {
		for( int i = 0; i < spaceship.getSpaceship().getLife(); i++) {	
			g.drawImage(life, 20 + (40 * i), 20 , 30, 30, null);
		}
	}
	
	/**
	 * définit l'affichage du New Game
	 * @param g
	 */
	public void drawStartGame(Graphics g) {
		if(startTimer != 0 && !start) {
			g.setFont(new Font("Spaceboy", Font.PLAIN, 30));
			String s = "- N E W _ G A M E - ";
			int length = (int)g.getFontMetrics().getStringBounds(s, g).getWidth();
			int alpha = (int) (255 * Math.sin(3.14 * startTimerDiff / startDelay ));
			if(alpha > 255) alpha = 255;
			g.setColor(new Color(255,255,255, alpha));
			g.drawString(s, Configuration.BOARD_WIDTH/2 - length / 2, Configuration.BOARD_HEIGHT / 2); 
		}
	}

	/*-----------------------------------------METHODES d'ACTION appelées dans le Update()-------------------------------------------------------*/

	/**
	 * Permet de redéfinir les variables du New Game
	 */
	public void newGame() {
		if(startTimer == 0) {
			start = false;
			startTimer = System.nanoTime();
			
		} else {
			startTimerDiff = (System.nanoTime() - startTimer) / 1000000;
			if(startTimerDiff > startDelay) {
				start = true;
				startTimer = 1;
				startTimerDiff = 0;
			}
		}
	}
	
	/**
	 * Permet de stopper les timers une fois qu'il est vérifié que le joueur est mort. Cette méthode lance également l'animation de l'explosion du vaison.
	 */
	public void checkDeadPlayer() {
		if(spaceshipControl.checkSpaceshipDeath()) {
			timerAnimationDeathShip.start();	
			timerAnimationDeathShip.setRepeats(false);
			counter++;
			if (counter == 100) {
				timerRefresh.setRepeats(false);
				timerAnimationMeteor.setRepeats(false);
			}
		}
	}

}
