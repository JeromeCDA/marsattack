package fr.afpa.marsattack.vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.net.URL;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import fr.afpa.marsattack.controller.GameController;
import fr.afpa.marsattack.model.GestionPlayerServices;
import fr.afpa.marsattack.model.GestionSpaceshipServices;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UiNewPlayer extends JFrame implements MouseListener, WindowListener {
	
	
	private JLabel nom;
	private JButton startButton;
	private JTextField nameField;
	private JPanel mainPanel;
	private JPanel startPanel;
	private JPanel blankPanelNorth;
	private JPanel blankPanelSouth;
	private JPanel west;
	private JPanel east;
	
	private GameController gameController;
	private GestionSpaceshipServices gSpaceship;
	private GestionPlayerServices gPlayer;
	private Game game;
	
	private int posX = 0;   //Position X de la souris au clic
    private int posY = 0;   //Position Y de la souris au clic
	
	public UiNewPlayer(GestionSpaceshipServices gSpaceship, Game game, GestionPlayerServices gPlayer){
		this.gPlayer = gPlayer;
		this.game = game;
        init();
	}
	
	
	public void init() {
		
		mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		
		
		nom = new JLabel("Meteor Ass-kicker name : ");
		nom.setForeground(new java.awt.Color(253, 80, 78));
		nom.setFont(new Font("Spaceboy", Font.PLAIN, 18));
		
		Border border = new LineBorder(new java.awt.Color(253, 80, 78), 1, true);
		nameField = new JTextField();
		nameField.setBorder(border);


		
		URL url = this.getClass().getResource("/images/spaceInvaders.gif");
//		ImageIcon imageIcon = new ImageIcon(url);
//		JLabel label = new JLabel(imageIcon);
		
		startButton = new JButton("Let's kick 'em all !");
		startButton.setName("Play");
		startButton.addMouseListener(this);

		
		
		
		blankPanelNorth = new JPanel();
		blankPanelNorth.setLayout(new GridLayout(3, 1));
		blankPanelNorth.add(new JLabel("                    "));
		blankPanelNorth.add(new JLabel("                    "));
//		blankPanelNorth.add(label);
		
		
		//west
		west = new JPanel();
		west.setLayout(new GridLayout(4,1));
		west.add(new JLabel("                    "));
		west.add(new JLabel("                    "));
		west.add(new JLabel("                    "));
		west.add(new JLabel("                    "));
		
		
		//east
		east = new JPanel();
		east.setLayout(new GridLayout(4,1));
		east.add(new JLabel("                    "));
		east.add(new JLabel("                    "));
		east.add(new JLabel("                    "));
		east.add(new JLabel("                    "));
		
		
		//center
		startPanel = new JPanel();
		startPanel.setLayout(new GridLayout(5, 2));
		startPanel.add(new JLabel("                    "));
		startPanel.add(new JLabel("                    "));
		startPanel.add(new JLabel("                    "));
		startPanel.add(new JLabel("                    "));
		startPanel.add(nom);
		startPanel.add(nameField);
		startPanel.add(new JLabel("                    "));
		startPanel.add(new JLabel("                    "));
		startPanel.add(new JLabel("                    "));
		startPanel.add(new JLabel("                    "));
		
		
		blankPanelSouth = new JPanel();
		blankPanelSouth.setLayout(new GridLayout(1, 3));
		blankPanelSouth.add(Box.createHorizontalGlue());
		blankPanelSouth.add(startButton);
		blankPanelSouth.add(Box.createHorizontalGlue());
		
		blankPanelSouth.setBorder(BorderFactory.createEmptyBorder(20,20,20,20));
		
		
		mainPanel.add(blankPanelNorth, BorderLayout.NORTH);
		mainPanel.add(startPanel, BorderLayout.CENTER);
		mainPanel.add(blankPanelSouth, BorderLayout.SOUTH);
		mainPanel.add(west, BorderLayout.WEST);
		mainPanel.add(east, BorderLayout.EAST);
		
		
		this.add(mainPanel);

		
		
		// A TAPER DANS GAME
		this.addWindowListener(this);
		
		//Retire le design par défaut
		//this.setUndecorated(true);
		
		this.setSize(564, 600);
		this.setLocationRelativeTo(null);
		this.getContentPane().setBackground(new java.awt.Color(40, 41, 40));
		
		
		
		mainPanel.setOpaque(false);
		blankPanelNorth.setOpaque(false);
		startPanel.setOpaque(false);
		blankPanelSouth.setOpaque(false);
		west.setOpaque(false);
		east.setOpaque(false);
		this.setVisible(true);
	
		
		
		addMouseListener(new MouseAdapter() {
            @Override
            //on récupère les coordonnées de la souris
            public void mousePressed(MouseEvent e) {
                posX = e.getX();    //Position X de la souris au clic
                posY = e.getY();    //Position Y de la souris au clic
            }
        });
         
        addMouseMotionListener(new MouseMotionAdapter() {
            // A chaque deplacement on recalcule le positionnement de la fenêtre
            @Override
            public void mouseDragged(MouseEvent e) {
                int depX = e.getX() - posX;
                int depY = e.getY() - posY;
                setLocation(getX()+depX, getY()+depY);
            }
        });
        
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		
		if(((JButton)e.getSource()).getName().equals("Play")) {
			gameController = new GameController(gSpaceship, game, gPlayer);
			if(gameController.checkPlayerName(nameField.getText())) {
				game.initVariable();
				this.setVisible(false);
				this.dispose();
			}else {
				wrongNameFormatDialog();
			}
		}	
	}
	
	public void wrongNameFormatDialog() {
		this.setVisible(false);
		JFrame f = new JFrame();
		Object[] options = {"Saisir de nouveau"};
		int option = JOptionPane.showOptionDialog(null,
                "Mauvais format. Min 3 / Max 6 caractères",
                "New Game",
                JOptionPane.DEFAULT_OPTION,
                JOptionPane.PLAIN_MESSAGE, null, options, options[0]);
		if (option == JOptionPane.OK_OPTION) {
			//appelle de nouveau la frame de création de player lorsque le joueur appuye sur le bouton
			this.setVisible(false);
		}
	}


	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void windowClosed(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void windowClosing(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	
	
	
	
	


}
