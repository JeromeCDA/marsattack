package fr.afpa.marsattack.vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

import fr.afpa.marsattack.beans.Player;
import fr.afpa.marsattack.controller.GameController;
import fr.afpa.marsattack.controller.MeteorController;
import fr.afpa.marsattack.controller.SpaceshipController;
import fr.afpa.marsattack.model.GestionMeteorServices;
import fr.afpa.marsattack.model.GestionPlayerServices;
import fr.afpa.marsattack.model.GestionSpaceshipServices;
import fr.afpa.marsattack.resources.Configuration;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter

/**
 * La classe Game est la classe principale qui contient les différents panels ( GameScene pour la scene du jeu, panelGameOver pour l'affichage du restart, et le panel d'ajout de joueur)
 * Elle possede un timer qui permet d'écouter ce qui se passe dans GameScene et de détecter si la variable static GameOver est passée à true.
 * Si c'est le cas, elle enclenche le Controller  GameController qui va modifier le contentPane de Game afin d'afficher le panelGameOver et enclenche la méthode init( pour réinitialiser la partie sur le joueur appuie sur restart.
 * 
 * @author NEBULA
 *
 */
public class Game extends JFrame {
	
	private GameScene gameScene;
	private GestionSpaceshipServices gSpaceship;
	private GestionMeteorServices gMeteors;
	private SpaceshipController spaceshipControl;
	private MeteorController meteorsControl;
	
	private GameController gameController;
	
	private Timer timerGameOver;

	private UiNewPlayer newPlayer;
	private JPanel panelGameOver;
	private JLabel mort;
	private JButton restart;
	private JButton score;
	private JButton exit;
	private JPanel pButton;
	
	private GestionPlayerServices gPlayer;


	
	public Game() {
		gameInit();
	}

	
	/**
	 * Methode servant d'initialisateur pour notre classe.
	 */
	public void gameInit() {
		

		// INIT VARIABLE NEW PLAYER
		gSpaceship = new GestionSpaceshipServices();
		gPlayer = new GestionPlayerServices();
		gameController = new GameController(gSpaceship, this, gPlayer);
		newPlayer = new UiNewPlayer(gSpaceship, this, gPlayer);

		
		
		initGameOverPanel(newPlayer);
		// A balancer après le top du bouton INITVARIABLE GAMESCENE
		//initVariable();

		setTitle(Configuration.TITLE);
		setSize(Configuration.BOARD_WIDTH, Configuration.BOARD_HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(false);
		setAlwaysOnTop(true);

		// A balancer après le top du bouton
		//setContentPane(gameScene);
		//setContentPane(newPlayer);
		pack();
		setVisible(false);
	}

	/**
	 * Initialise les variables de la classe et le timer écouteur de GameOver
	 */
	public void initVariable() {
		
		gMeteors = new GestionMeteorServices(gSpaceship);
		spaceshipControl = new SpaceshipController(gSpaceship, gMeteors, gameScene, this);
		meteorsControl = new MeteorController(gSpaceship, gMeteors, spaceshipControl, gameScene);
		gameScene = new GameScene(gSpaceship, gMeteors, spaceshipControl, meteorsControl );

		
		this.addKeyListener(spaceshipControl);
		this.setFocusable(true);
		this.requestFocusInWindow();
		this.getNewPlayer().dispose();
		//ECOUTEUR DE GAME OVER
		timerGameOver = new Timer(700, gameController);
		timerGameOver.start();	
		
		this.setContentPane(gameScene);
		
		pack();
		this.setVisible(true);
	}

	
	/**
	 * Comme son nom l'indique, cette méthode permet l'initialisation du panelGameOver
	 */
	public void initGameOverPanel(UiNewPlayer newPlayer) {
		panelGameOver = new JPanel();
		panelGameOver.setLayout(new BorderLayout());
		mort = new JLabel("Vous etes mort !");
			mort.setFont(new Font("Spaceboy", Font.PLAIN, 20));
			mort.setHorizontalAlignment(JLabel.CENTER);
			mort.setForeground(Color.WHITE);
		pButton = new JPanel(new FlowLayout());
			restart = new JButton("Restart");		
			score = new JButton("Score");
			exit = new JButton("Exit");	
			restart.addActionListener( gameController);
			exit.addActionListener(gameController);
			score.addActionListener(gameController);
			pButton.add(restart);
			pButton.add(score);
			pButton.add(exit);
		panelGameOver.setSize(Configuration.BOARD_WIDTH, Configuration.BOARD_HEIGHT);
		panelGameOver.add(mort, BorderLayout.CENTER);
		panelGameOver.add(pButton, BorderLayout.SOUTH);
		panelGameOver.setBackground(Color.black);
	}
	
	public void loopThroughScoresList() {
		System.out.println("Jjojojo");
        this.setVisible(false);
        JFrame f = new JFrame();
        ArrayList <Player> listAllScores = gameController.retrieveScoresFromModel();
        StringBuilder sb = new StringBuilder();
        System.out.println("Ma liste "+listAllScores);
        for (int i = 0 ; i < listAllScores.size() ; i++) {
        	if (i == 20) {
        		break;
        	}
            sb.append(listAllScores.get(i).getName()+";"+listAllScores.get(i).getScore()+";"+listAllScores.get(i).getGameOverGetTime()+"\n");
        }
            System.out.println("Dans la boucle "+sb);
        Object[] options = {"Back to menu"};
        int option = JOptionPane.showOptionDialog(null,
            sb,
            "Scores : ",
            JOptionPane.DEFAULT_OPTION,
            JOptionPane.PLAIN_MESSAGE, null, options, options[0]);
        if (option == JOptionPane.OK_OPTION) {
        	this.setVisible(true);
        }
    
    }
	
	//aucun score pour le moment
    public void noScoreToDisplay() {
        this.setVisible(false);
        JFrame f = new JFrame();
        Object[] options = {"Back to menu"};
        int option = JOptionPane.showOptionDialog(null,
                "No score to display. Play now !",
                "Scores : ",
                JOptionPane.DEFAULT_OPTION,
                JOptionPane.PLAIN_MESSAGE, null, options, options[0]);
        if (option == JOptionPane.OK_OPTION) {
            //retourne au menu
        }
    }


	
	public static void main (String [] args) {
		
		new Game();
	}
}
