package fr.afpa.marsattack.beans;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import fr.afpa.marsattack.resources.Configuration;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
/**
 * Classe mere des météores. Elle peut être instanciée et représente la météore basique dans le jeu.
 * @author NEBULA
 *
 */
public class Meteor extends Entity {

	protected Image currentImage;

	protected int impact;
	protected int points;
	
	// est nécessaire pour lancer l'animation des météores
	protected boolean un; protected boolean deux; protected boolean trois; 	protected boolean quatre; 	protected boolean cinq; protected boolean six; 	protected boolean sept; protected boolean huit;
	protected boolean neuf;	protected boolean dix;	protected boolean onze; protected boolean douze;
	
	public Meteor() {
		initialize();
	}
	
	public void initialize() {
		this.setYPos((randXYMeteor(0,400)) * -1);
		this.setXPos(randXYMeteor(50, Configuration.BOARD_HEIGHT-50));
		this.setHeight(Configuration.B_METEOR_HEIGTH);
		this.setWidth(Configuration.B_METEOR_WIDTH);
		this.setDx(0);
		this.setDy(0);
		this.speed =12;
		this.impact = 1;
		this.points = 2;
		un = true;	deux = false; trois = false;quatre = false;cinq = false;six = false;sept = false;huit = false;neuf = false;	dix = false;onze = false;douze = false;

		ImageIcon image = new ImageIcon(getClass().getResource("/Meteor/simpleMeteor/simpleMeteor_0.png"));
		this.ImgMov1 = image.getImage(); 
		image = new ImageIcon(getClass().getResource("/Meteor/simpleMeteor/simpleMeteor_1.png"));
		this.ImgMov2 = image.getImage(); 
		image = new ImageIcon(getClass().getResource("/Meteor/simpleMeteor/simpleMeteor_2.png"));
		this.ImgMov3 = image.getImage(); 
		image = new ImageIcon(getClass().getResource("/Meteor/simpleMeteor/simpleMeteor_3.png"));
		this.ImgMov4 = image.getImage(); 
		image = new ImageIcon(getClass().getResource("/Meteor/simpleMeteor/simpleMeteor_4.png"));
		this.ImgMov5 = image.getImage(); 
		image = new ImageIcon(getClass().getResource("/Meteor/simpleMeteor/simpleMeteor_5.png"));
		this.ImgMov6 = image.getImage(); 
		image = new ImageIcon(getClass().getResource("/Meteor/simpleMeteor/simpleMeteor_6.png"));
		this.ImgMov7 = image.getImage(); 
		image = new ImageIcon(getClass().getResource("/Meteor/simpleMeteor/simpleMeteor_7.png"));
		this.ImgMov8 = image.getImage(); 
		image = new ImageIcon(getClass().getResource("/Meteor/simpleMeteor/simpleMeteor_8.png"));
		this.ImgMov9 = image.getImage(); 
		image = new ImageIcon(getClass().getResource("/Meteor/simpleMeteor/simpleMeteor_9.png"));
		this.ImgMov10 = image.getImage(); 
		image = new ImageIcon(getClass().getResource("/Meteor/simpleMeteor/simpleMeteor_10.png"));
		this.ImgMov11 = image.getImage(); 
		image = new ImageIcon(getClass().getResource("/Meteor/simpleMeteor/simpleMeteor_11.png"));
		this.ImgMov12 = image.getImage(); 

		this.currentImage = ImgMov1;
	}
	

	/**
	 * Permet de calculer un genre de carré autour de notre météores. Cette méthode va servir a la déctection des collisions.
	 */
	public void calculedBox() {
		leftSide = this.xPos;
		rightSide = this.xPos + this.width-10;
		topSide = this.yPos;
		bottomSide = this.yPos + this.height-10;
	}

	/**
	 * Cette méthode centralise tous les changements qui vont s'effectuer pour notre météore. Elle sera appelée par le model et par le controller.
	 */
	public void update() {
		this.calculedBox();
		this.setYPos(this.getYPos()+ speed);
	}

	/**
	 * Cette méthode permet de dessiner la météores avec ses propres caractéristiques. Elle est appelée par le model et le controller.
	 * @param g
	 */
	public void draw(Graphics2D g) {
		g.drawImage(this.currentImage, this.xPos, this.yPos, this.height,this.width, null);
	}
	
	/**
	 * Permet de donner une position random a notre météore.
	 * @param x1
	 * @param x2
	 * @return
	 */
	public int randXYMeteor(int x1, int x2) {
		Random r = new Random();
		return r.nextInt((x2 - x1) + 1) + x1;
	}

	
	/**
	 * Permet l'animation des météores.
	 */
	public void animatedImages() {
		if (un) {
			this.setCurrentImage(ImgMov1);
			un = false;
			deux = true;
		}else if(deux) {
			this.setCurrentImage(this.ImgMov2);
			deux = false;
			trois = true;
		}else if (trois) {
			this.setCurrentImage(this.ImgMov3);
			trois = false;
			quatre = true;
		}else if(quatre) {
			this.setCurrentImage(this.ImgMov4);
			quatre = false;
			cinq = true;
		}else if(cinq) {
			this.setCurrentImage(this.ImgMov5);
			cinq = false;
			six = true;
		}else if(six) {
			this.setCurrentImage(this.ImgMov6);
			six = false;
			sept = true;
		}else if(sept) {
			this.setCurrentImage(this.ImgMov7);
			sept = false;
			huit = true;
		}else if(huit) {
			this.setCurrentImage(this.ImgMov8);
			huit = false;
			neuf = true;
		}else if(neuf) {
			this.setCurrentImage(this.ImgMov9);
			neuf = false;
			dix = true;
		}else if(dix) {
			this.setCurrentImage(this.ImgMov10);
			dix = false;
			onze = true;
		}else if(onze) {
			this.setCurrentImage(this.ImgMov11);
			onze = false;
			douze = true;
		}else if(douze) {
			this.setCurrentImage(this.ImgMov12);
			douze = false;
			un = true;
		}
	}

}
