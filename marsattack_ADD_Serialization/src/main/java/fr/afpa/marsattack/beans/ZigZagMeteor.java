package fr.afpa.marsattack.beans;

import java.util.Random;

import javax.swing.ImageIcon;


public class ZigZagMeteor extends Meteor {

	private boolean right;
	private boolean left;
	private int nb;
	
	public void initialize() {
		super.initialize();
		this.speed = 9;
		this.impact = 2;
		this.points = 4;
		right = false;
		left = true;
		nb = 0;

		ImageIcon image = new ImageIcon(getClass().getResource("/Meteor/ZigZag/zigzagMeteor_00000.png"));
		this.ImgMov1 = image.getImage(); 
		image = new ImageIcon(getClass().getResource("/Meteor/ZigZag/zigzagMeteor_00001.png"));
		this.ImgMov2 = image.getImage(); 
		image = new ImageIcon(getClass().getResource("/Meteor/ZigZag/zigzagMeteor_00002.png"));
		this.ImgMov3 = image.getImage(); 
		image = new ImageIcon(getClass().getResource("/Meteor/ZigZag/zigzagMeteor_00003.png"));
		this.ImgMov4 = image.getImage(); 
		image = new ImageIcon(getClass().getResource("/Meteor/ZigZag/zigzagMeteor_00004.png"));
		this.ImgMov5 = image.getImage(); 
		image = new ImageIcon(getClass().getResource("/Meteor/ZigZag/zigzagMeteor_00005.png"));
		this.ImgMov6 = image.getImage(); 
		image = new ImageIcon(getClass().getResource("/Meteor/ZigZag/zigzagMeteor_00006.png"));
		this.ImgMov7 = image.getImage(); 
		image = new ImageIcon(getClass().getResource("/Meteor/ZigZag/zigzagMeteor_00007.png"));
		this.ImgMov8 = image.getImage(); 
		image = new ImageIcon(getClass().getResource("/Meteor/ZigZag/zigzagMeteor_00008.png"));
		this.ImgMov9 = image.getImage(); 
		image = new ImageIcon(getClass().getResource("/Meteor/ZigZag/zigzagMeteor_00009.png"));
		this.ImgMov10 = image.getImage(); 
		image = new ImageIcon(getClass().getResource("/Meteor/ZigZag/zigzagMeteor_00010.png"));
		this.ImgMov11 = image.getImage(); 
		image = new ImageIcon(getClass().getResource("/Meteor/ZigZag/zigzagMeteor_00011.png"));
		this.ImgMov12 = image.getImage(); 
		
		this.setCurrentImage(ImgMov1);
	}

	
    public void moveY() {
    	this.setYPos(this.getYPos()+ speed);
    }
    
    /**
     * Permet de faire le zigzag
     */
    public void moveX() {
    	if ( getYPos() > 0) {
    		int meteorSwitchSide=Integer.parseInt(Integer.toString(this.getYPos()).substring(0, 1));

             if (this.getYPos() >= 0 && this.getYPos() < 100) {
                 this.xPos = this.xPos + this.speed;
             }

             if(this.getYPos() > 99 && meteorSwitchSide % 2 != 0) {
                 this.xPos = this.xPos - this.speed;

             } else if (this.getYPos() > 99 && meteorSwitchSide % 2 == 0) {
                 this.xPos = this.xPos + this.speed;
             }
    	}
    }
    
//    public void move() {
//        xPos += dx;
//        yPos += dy;
//
//        yPos = yPos+1;
//
//        if(yPos % 2 ==0) {
//            xPos = xPos + randInt(-10,0);
//            }
//        else {
//            xPos = xPos + randInt(0,10);
//        }
          //pour l'autre technique je dois en gros à y == 200 tuer la météorite et refaire apparaitre une autre une nouvelle à ce point avec la position et la rand (à faire 3 fois    
//    }
    
    //x je fais un rand entre 50 et 510 et y +1
    public int randInt(int min, int max) {

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;


}

	@Override
	public void update() {
		this.calculedBox();
//		move();
		moveY();
		moveX();
	}
}