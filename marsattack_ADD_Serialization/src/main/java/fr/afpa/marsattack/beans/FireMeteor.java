package fr.afpa.marsattack.beans;


import javax.swing.ImageIcon;



public class FireMeteor extends Meteor {
	@Override
	public void initialize() {
		super.initialize();
		this.width = 80;
		this.height = 80;
		this.speed = 7;
		this.impact = 2;
		this.points = 1;
		this.un = true;

		ImageIcon image = new ImageIcon(getClass().getResource( "/Meteor/fireMeteor/FireM_0.png"));
		this.ImgMov1 = image.getImage(); 
		image = new ImageIcon(getClass().getResource("/Meteor/fireMeteor/FireM_1.png"));
		this.ImgMov2 = image.getImage(); 
		image = new ImageIcon(getClass().getResource("/Meteor/fireMeteor/FireM_2.png"));
		this.ImgMov3 = image.getImage(); 
		image = new ImageIcon(getClass().getResource("/Meteor/fireMeteor/FireM_3.png"));
		this.ImgMov4 = image.getImage(); 
		image = new ImageIcon(getClass().getResource("/Meteor/fireMeteor/FireM_4.png"));
		this.ImgMov5 = image.getImage(); 
		image = new ImageIcon(getClass().getResource("/Meteor/fireMeteor/FireM_5.png"));
		this.ImgMov6 = image.getImage(); 
		image = new ImageIcon(getClass().getResource("/Meteor/fireMeteor/FireM_6.png"));
		this.ImgMov7 = image.getImage(); 
		image = new ImageIcon(getClass().getResource("/Meteor/fireMeteor/FireM_7.png"));
		this.ImgMov8 = image.getImage(); 
		image = new ImageIcon(getClass().getResource("/Meteor/fireMeteor/FireM_8.png"));
		this.ImgMov9 = image.getImage(); 
		image = new ImageIcon(getClass().getResource("/Meteor/fireMeteor/FireM_9.png"));
		this.ImgMov10 = image.getImage(); 
		image = new ImageIcon(getClass().getResource("/Meteor/fireMeteor/FireM_10.png"));
		this.ImgMov11 = image.getImage(); 
		image = new ImageIcon(getClass().getResource("/Meteor/fireMeteor/FireM_11.png"));
		this.ImgMov12 = image.getImage(); 
	}

}


	

