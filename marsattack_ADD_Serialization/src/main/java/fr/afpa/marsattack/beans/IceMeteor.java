package fr.afpa.marsattack.beans;


import javax.swing.ImageIcon;

public class IceMeteor extends Meteor {

	public void initialize() {
		super.initialize();
		this.speed = 12;
		this.impact = 2;
		this.points = 3;

		
		ImageIcon image = new ImageIcon(getClass().getResource( "/Meteor/icedMeteor/iceMetor_0_00000.png"));
		this.ImgMov1 = image.getImage(); 
		image = new ImageIcon(getClass().getResource( "/Meteor/icedMeteor/iceMetor_0_00001.png"));
		this.ImgMov2 = image.getImage(); 
		image = new ImageIcon(getClass().getResource( "/Meteor/icedMeteor/iceMetor_0_00002.png"));
		this.ImgMov3 = image.getImage(); 
		image = new ImageIcon(getClass().getResource( "/Meteor/icedMeteor/iceMetor_0_00003.png"));
		this.ImgMov4 = image.getImage(); 
		image = new ImageIcon(getClass().getResource( "/Meteor/icedMeteor/iceMetor_0_00004.png"));
		this.ImgMov5 = image.getImage(); 
		image = new ImageIcon(getClass().getResource( "/Meteor/icedMeteor/iceMetor_0_00005.png"));
		this.ImgMov6 = image.getImage(); 
		image = new ImageIcon(getClass().getResource( "/Meteor/icedMeteor/iceMetor_0_00006.png"));
		this.ImgMov7 = image.getImage(); 
		image = new ImageIcon(getClass().getResource( "/Meteor/icedMeteor/iceMetor_0_00007.png"));
		this.ImgMov8 = image.getImage(); 
		image = new ImageIcon(getClass().getResource( "/Meteor/icedMeteor/iceMetor_0_00008.png"));
		this.ImgMov9 = image.getImage(); 
		image = new ImageIcon(getClass().getResource( "/Meteor/icedMeteor/iceMetor_0_00009.png"));
		this.ImgMov10 = image.getImage(); 
		image = new ImageIcon(getClass().getResource( "/Meteor/icedMeteor/iceMetor_0_00010.png"));
		this.ImgMov11 = image.getImage(); 
		image = new ImageIcon(getClass().getResource( "/Meteor/icedMeteor/iceMetor_0_00011.png"));
		this.ImgMov12 = image.getImage(); 
		
		this.setCurrentImage(ImgMov1);
		
	}

}