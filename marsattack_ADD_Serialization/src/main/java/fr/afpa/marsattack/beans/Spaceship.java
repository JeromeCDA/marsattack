package fr.afpa.marsattack.beans;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import fr.afpa.marsattack.resources.Configuration;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
/**
 * Classe représentant le vaisseau.
 * @author NEBULA
 *
 */
public class Spaceship extends Entity {

	// image current et de desctruciton.
	private Image dest1,dest2, dest3, dest4, dest5, dest6, dest7, dest8;
	private boolean un, deux, trois, quatre, cinq, six, sept, huit;
	private Image blank; 
	private Image collisionImg;
	
	// etat
	private boolean collisionMeteor;
	private long recoveryTimer;
	

	private int score;
	
	
	public Spaceship() {
		initialize();
		initializeImage();
	}
	
	
	public void initialize() {
		
		this.life = 5; 
		this.speed = 10;
		this.score = 0;
		
		this.xPos = Configuration.X_POS_INIT_SHIP;
		this.yPos = Configuration.Y_POS_INIT_SHIP;
		this.height = Configuration.SPACESHIP_HEIGTH;
		this.width = Configuration.SPACESHIP_WIDTH;
		
		this.dx = 0;
		this.dy =0;	
		
		collisionMeteor = false;
		recoveryTimer = 0;
	
		this.un = true; this.deux = false; this.trois = false; this.quatre = false; this.cinq = false; this.six = false; this.sept = false; this.huit = false;
		
		// permet d'initialiser le point d'ancrage au milieu de notre spaceShip
		int start_x = Configuration.BOARD_WIDTH/2-Configuration.SPACESHIP_WIDTH/2;
		int start_y = Configuration.BOARD_HEIGHT-145;
		this.xPos = start_x;
		this.yPos = start_y;
	}
	
	public void initializeImage() {

		ImageIcon image = new ImageIcon(getClass().getResource("/FRONT_Ship1.png"));
		this.image = image.getImage(); 
		ImgMov1 = image.getImage();
		image = new ImageIcon(getClass().getResource("/LEFT_Ship1.png"));
		ImgMov2 = image.getImage();
		image = new ImageIcon(getClass().getResource("/RIGHT_Ship1.png"));
		ImgMov3 = image.getImage();
		
		image = new ImageIcon(getClass().getResource("/SpaceShip/xplode/xplode_1.png"));
		dest1 = image.getImage();
		image = new ImageIcon(getClass().getResource("/SpaceShip/xplode/xplode_2.png"));
		dest2 = image.getImage();
		image = new ImageIcon(getClass().getResource("/SpaceShip/xplode/xplode_3.png"));
		dest3 = image.getImage();
		image = new ImageIcon(getClass().getResource("/SpaceShip/xplode/xplode_4.png"));
		dest4 = image.getImage();
		image = new ImageIcon(getClass().getResource("/SpaceShip/xplode/xplode_5.png"));
		dest5 = image.getImage();
		image = new ImageIcon(getClass().getResource("/SpaceShip/xplode/xplode_6.png"));
		dest6 = image.getImage();
		image = new ImageIcon(getClass().getResource("/SpaceShip/xplode/xplode_7.png"));
		dest7 = image.getImage();
		image = new ImageIcon(getClass().getResource("/SpaceShip/xplode/xplode_8.png"));
		dest8 = image.getImage();
		
		image = new ImageIcon(getClass().getResource("/COLLISION.png"));
		collisionImg = image.getImage();

	}
	
	
	

}
