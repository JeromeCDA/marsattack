package fr.afpa.marsattack.beans;



import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Player implements Comparable<Player>, Serializable {
	
	
	private int score;
	private String name;
	private String gameOverGetTime;
	
	public Player() {
//		this.name = "R2D2"; 
	}
	public Player(String name) {
		this.name = name;
		LocalDateTime getTime = LocalDateTime.now();
        DateTimeFormatter frenchFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        this.gameOverGetTime = getTime.format(frenchFormat);
		
	}
	
	@Override
    public int compareTo(Player player) {
        return this.getScore()-player.getScore();
    }

	@Override
	public String toString() {
		return "Player [score=" + score + ", name=" + name + ", gameOverGetTime=" + gameOverGetTime + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Player other = (Player) obj;
		if (gameOverGetTime == null) {
			if (other.gameOverGetTime != null)
				return false;
		} else if (!gameOverGetTime.equals(other.gameOverGetTime))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (score != other.score)
			return false;
		return true;
	}
	
	
	

}
