package fr.afpa.marsattack.beans;


import java.awt.Image;

import javax.swing.ImageIcon;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
/**
 * Cette classe est la classe mere de tout nos beans.
 * @author NEBULA
 *
 */
public abstract class Entity {

	// images
	protected Image image;
	protected ImageIcon icon;
	protected Image ImgMov1, ImgMov2, ImgMov3, ImgMov4,ImgMov5,ImgMov6,ImgMov7,ImgMov8,ImgMov9,ImgMov10, ImgMov11, ImgMov12;
	protected Image deadImg;
	protected String pathImgDest1;
	
	//etat
	protected boolean collision;
	protected boolean dead;
	protected int life;
	
	// position
	protected int xPos;
	protected int yPos;
	protected int dx;
	protected int dy;
	protected int speed;
	//taille
	protected int height;
	protected int width;
	
	// mouvement
	boolean left;
	boolean right;
	boolean up;
	boolean down;
	
	// Calcul du rectangle
	protected int leftSide;
	protected int rightSide;
	protected int topSide;
	protected int bottomSide;
}
